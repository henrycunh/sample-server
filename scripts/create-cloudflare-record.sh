#!/bin/bash
$TARGET="$1"
[[ -z $TARGET ]] && echo "Missing record target" && exit 0

$NAME="$STAGE.server.cati"
PAYLOAD=$(echo '{ "type": "CNAME", "name": "'$NAME'", "content": "'$TARGET'", "ttl": 1 }')

curl \
    -X POST "https://api.cloudflare.com/client/v4/zones/$CLOUDFLARE_ZONE/dns_records" \
    -H "Authorization: Bearer $CLOUDFLARE_API_TOKEN" \
    -d"$PAYLOAD"
