#!/bin/bash
BASE_URL="https://api.cloudflare.com/client/v4"

RECORD_ID_RESPONSE=$(
    curl \ 
    -X GET "$BASE_URL/zones/$ZONE/dns_records" \
    -H "Authorization: Bearer $CLOUDFLARE_API_TOKEN"
)

$NAME="$STAGE.server.cati"
RECORD_ID=$(
    echo $RECORD_ID_RESPONSE | jq -c '.result[] | select( .name | contains( "'$NAME'" ))' | jq .id | tr -d \"
)

curl \
    -X DELETE "$BASE_URL/zones/$ZONE/dns_records/$RECORD_ID" \
    -H "Authorization: Bearer $CLOUDFLARE_API_TOKEN"
