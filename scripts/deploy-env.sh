#!/bin/bash
npx serverless deploy --stage $STAGE | tee .sls-output
export DEPLOYMENT_URL=$(cat .sls-output | grep "ANY - " -m1 | sed -e 's/ANY - //g' -e 's/ *$//g' -e 's/^ *//g | sed sed 's/\/[^\/]*$//g')
rm .sls-output