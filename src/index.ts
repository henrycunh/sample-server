import express from 'express'
import serverless from 'serverless-http'

const server = express()

server.get('/', (req, res) => {
    res.send('OK')
})

export const handler = serverless(server)
export { server } 