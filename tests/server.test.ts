import request from 'supertest'
import { server } from '../src/index'

describe("Test the endpoint", () => {
    test("should return OK", async () => {
        const response = await request(server).get('/')
        expect(response.statusCode).toBe(200)
        expect(response.text).toBe('OK')
    })
})